# frozen requirements generated by pip-deepfreeze
mypy==0.991
mypy-extensions==0.4.3
tomli==2.0.1
types-urllib3==1.26.25.4
